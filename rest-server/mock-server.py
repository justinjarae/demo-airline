import json
import web

class MyApplication(web.application):
    def run(self, port=8080, *middleware):
        func = self.wsgifunc(*middleware)
        return web.httpserver.runsimple(func, ('0.0.0.0', port))

urls = (
    '/', 'index',
    '/getArrivalInfo', 'getArrivalInfo',
    '/testing', 'testing'
)

class index:
    def GET(self):
        output = "Available endpoints:\n" \
                "/ - outputs this explaination\n" \
                "/getArrivalInfo - accepts a FlightInfo object and returns data filled in"
        return output


class getArrivalInfoUseInput:
    def POST(self):
        print ('web.data: ' + web.data())
        flightInfo = json.loads(web.data())
        flightInfo["arrivalGate"] = "E10"
        web.header('Content-Type', 'application/json')
        return json.dumps(flightInfo)


class getArrivalInfo:
    def POST(self):
        response = json.loads('{"aircraftType": "B738","arrivalAirport": "KMSP","arrivalGate":"E10","arrivalTime": "2018-03-21T13:00:00.000-07:00","departureAirport": "KDEN","departureGate": "B31","flightNumber": "2118"}')
        print ('web.data: ' + web.data())
        #flightInfo = json.loads(web.data())
        web.header('Content-Type', 'application/json')
        return json.dumps(response)

if __name__ == "__main__":
    app = MyApplication(urls, globals())
    app.run(port=3030)
